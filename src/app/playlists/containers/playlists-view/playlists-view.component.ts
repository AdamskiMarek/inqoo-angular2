import { Component, OnInit } from '@angular/core';
import { PlayList } from '../../PlayList';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss'],
})
export class PlaylistsViewComponent implements OnInit {
  mode: 'details' | 'edit' | 'create' = 'details';

  playlists: PlayList[] = [
    {
      id: '123',
      name: 'Playlist 123',
      public: true,
      description: 'my favourite playlist',
    },
    {
      id: '234',
      name: 'Playlist 234',
      public: false,
      description: 'my favourite ',
    },
    {
      id: '345',
      name: 'Playlist 34',
      public: true,
      description: 'my  playlist',
    },
  ];

  selected = this.playlists[1];

  constructor() {}

  // edit() {
  //   this.mode = 'edit';
  // }
  // draft: PlayList

  // createNew() {
  //   this.selected = this.draft
  // }

  save(draft:PlayList){

    this.playlists = this.playlists.map(function(p) {return p.id == draft.id? {...draft}:p} )
  }

  delete(draft:PlayList){
    // this.playlists = this.playlists.map(function(p) {return p.id == draft.id? this.playlists :p})
  }


  ngOnInit(): void {

  }

  // cancel() {
  //   this.mode = 'details';
  // }
}
