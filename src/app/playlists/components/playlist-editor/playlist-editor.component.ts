import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PlayList } from '../../PlayList';

@Component({
  selector: 'app-playlist-editor',
  templateUrl: './playlist-editor.component.html',
  styleUrls: ['./playlist-editor.component.scss'],
})
export class PlaylistEditorComponent implements OnInit {
  // playlist = {
  //   id: '123',
  //   name: 'Playlist 123',
  //   public: false,
  //   description: 'my favourite playlist'
  // }

  @Input() selected?: PlayList;

  @Output() selectedChange = new EventEmitter<PlayList>();

  @Input() mode: 'details' | 'edit' | 'create' = 'details';

  @Output() modeChange = new EventEmitter<'details' | 'edit' | 'create'>();

  @Output() deleteChange = new EventEmitter<PlayList>();

  cancel() {
    this.modeChange.emit('details');
  }

  draft: PlayList | undefined;

  save() {
   this.selectedChange.emit(this.draft)
  }

  delete() {
    this.deleteChange.emit(this.draft)
  }

  constructor() {}

  ngOnInit(): void {
    if (this.selected) {
      this.draft = { ...this.selected };
    }
  }
}
