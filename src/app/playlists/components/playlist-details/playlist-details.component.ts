import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PlayList } from '../../PlayList';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss'],
})
export class PlaylistDetailsComponent implements OnInit {
  // playlist = {
  //   id:'123',
  //   name:'Playlist 123',
  //   public:false,
  //   description:'my favourite playlist'
  // }

  @Input() selected?: PlayList;

  @Input() mode: 'details' | 'edit' | 'create' = 'details';

  @Output() modeChange = new EventEmitter<'details' | 'edit' | 'create'>();

  edit() {
    this.modeChange.emit('edit');
  }

  constructor() {}

  ngOnInit(): void {}
}
