import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PlayList } from '../../PlayList';

@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss'],
})
export class PlaylistsListComponent implements OnInit {
  @Input('items') playlists: PlayList[] = [];

  @Input() selected?: PlayList;

  @Output() selectedChange = new EventEmitter<PlayList>();

  select(playlist: PlayList) {
    //  this.selected = playlist;
    this.selectedChange.emit(playlist);
  }

  constructor() {}

  ngOnInit(): void {}
}
