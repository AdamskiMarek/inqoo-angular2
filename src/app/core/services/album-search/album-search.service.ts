import { Injectable } from '@angular/core';
import { Album } from '../../model/album';


const mockAlbums: Album[] = [
  {
    id: '123', name: 'Album 123', images: [
      { url: 'https://www.placecage.com/c/300/300' }
    ]
  },
  {
    id: '234', name: 'Album 234', images: [
      { url: 'https://www.placecage.com/c/400/400' }
    ]
  },
  {
    id: '345', name: 'Album 345', images: [
      { url: 'https://www.placecage.com/c/400/300' }
    ]
  },
  {
    id: '456', name: 'Album 456', images: [
      { url: 'https://www.placecage.com/c/300/400' }
    ]
  },
]
@Injectable({
  providedIn: 'root'
})
export class AlbumSearchService {

  getAlbums(): Album[] {
    return mockAlbums
  }

  constructor() { }
}
