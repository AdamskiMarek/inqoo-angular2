export interface Album {
  id: string,
  name: string,
  images: {
    url: string
  }[]
}
